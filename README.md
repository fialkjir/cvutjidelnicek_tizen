# CVUTjidelnicek_Tizen

Aplikace, která zobrazuje denní jídelníček vybrané ČVUT menzy ve formě widgetu. Jak už název napovídá, aplikace je určena pro chytré hodinky Samsung s operačním systémem Tizen Wearable(od verze 5.5 a vyšší).

## Bude aplikace dostupná na Samsung Galaxy Store?

Až se mi podaří splnit všechny podmínky pro nahrání a aplikace bude chválena, tak ano. Již brzy ;-)

## Proč je jídelníček ve formě widgetu a né aplikace?
Původní myšlenkou byl, co nejrychlejší přístup k jídelníčku, avšak během vývoje se ukázala volba widgetu jako celkem slepá ulička. Widget nepodporuje slider ani jiný způsob scrollování. Ale nakonec jsem se vypořádal s omezeným prostorem. Do budoucna vytvořím i aplikaci nebo kombinaci aplikace a widgetu(Tedy ve widgetu by se zobrazovala třeba jen hlavní jídla a zbytek po rozkliknutí v aplikaci).

## Ukázka widgetu
![aplikacefotka](/uploads/93a0997eb45d15cc16d9ce6e4add4e36/aplikacefotka.png)![Screenshot_from_2023-08-24_22-17-56](/uploads/a32b87bb1eeb65262f651e679537c0bf/Screenshot_from_2023-08-24_22-17-56.png)![Screenshot_from_2023-08-24_22-18-17](/uploads/501d8838d8dc153c67b3f1de6ade5332/Screenshot_from_2023-08-24_22-18-17.png)![Screenshot_from_2023-08-24_22-18-26](/uploads/f1b5b23ed970dbf1f4215792eb3132f9/Screenshot_from_2023-08-24_22-18-26.png)

## Použité knihony, api
Aplikace využívá API(dostupné po kontaktování Oddělení gastronomických služeb - IT) pro načítání jídelníčku. Dále jsou použity knihovny libcurl a json-c.