#ifndef __cvutjidelnicek_H__
#define __cvutjidelnicek_H__

#include <curl.h>
#include <widget_app.h>
#include <widget_app_efl.h>
#include <Elementary.h>
#include <dlog.h>
#include <string.h>
#include "zpracovaniDat.h"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "cvutjidelnicek"

#if !defined(PACKAGE)
#define PACKAGE "org.jirif.cvutjidelnicek"
#endif

#endif /* __cvutjidelnicek_H__ */
