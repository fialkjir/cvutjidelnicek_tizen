/*
 * zpracovaniDat.h
 *
 *  Created on: Jun 3, 2023
 *      Author: jirka
 */

#ifndef ZPRACOVANIDAT_H_
#define ZPRACOVANIDAT_H_

#include <curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlog.h>
#include "cJSON.h"
//struktury
typedef struct KATEGORIE{
  char* nazev;
  int id;
} KATEGORIE;
//globalni promene
KATEGORIE* kategorie;
char** jidlaRadky;
int pocet_radku;
int pocet_kategorii;
//konstanty
const int MAX_DELKA_RADKU;
const int MAX_RADKU;
const int ID_STUDENSKA;
const int ID_TECHNICKA;
const char KLIC[11];
const int MAX_DELKA_URL;
const char ZACATEK_URL[69];
//deklarace funkci a metod
void vlozeniRadku (char* castjidla);
void kopiruj (char* dest, char* src, int zacatek, int konec);
void upraveniNazvu (char* jidlo);
void vymazPoleRetezcu (char** pole_retezcu, int velikost_pole);
void nactiJidla (int podsystem, int kategorie);
void nactiKategorie (int podsystem);
int najdiID(char* kat);

#endif /* ZPRACOVANIDAT_H_ */
