/*
 * Curl.h
 *
 *  Created on: Jun 2, 2023
 *      Author: jirka
 */

#ifndef CURL_H_
#define CURL_H_

#include <stdio.h>
#include <tizen.h>
#include <string.h>
#include <Ecore.h>
#include <curl/curl.h>
#include <net_connection.h>
#include <dlog.h>

struct MemoryStruct {
	char *memory;
	size_t size;
};
struct MemoryStruct chunk;
//deklarace funkci a metod
void nactiUrl(char* url);


#endif /* CURL_H_ */
