#include "zpracovaniDat.h"

//globalni promenne
char** jidlaRadky; //finalni seznam jidel upravenych do radku
int pocet_radku = 0;
int pocet_kategorii = 0;
KATEGORIE* kategorie;
//konstanty
const int MAX_DELKA_RADKU = 56;
const int MAX_RADKU = 13;
const int ID_STUDENSKA = 2;
const int ID_TECHNICKA = 3;
const char KLIC[] = "Jocf525x91";
const int MAX_DELKA_URL = 160;
const char ZACATEK_URL[] = "https://agata-new.suz.cvut.cz/jidelnicky/JAPIV2/json_API.php?Funkce=";

void vlozeniRadku (char* castjidla) {
    pocet_radku++;
    char* radek = calloc(strlen(castjidla)+15, sizeof(char));
    strcat(radek, "<font_size=14>");
    strcat(radek, castjidla);
    jidlaRadky = realloc(jidlaRadky, pocet_radku*sizeof(char*));
    jidlaRadky[pocet_radku-1] = radek;
}

void kopiruj (char* dest, char* src, int zacatek, int konec) {
    char retezec[(konec-zacatek)+1];
    for (int i=zacatek;i<konec+1;i++) {
        retezec[i-zacatek]=src[i];
    }
    retezec[(konec-zacatek)]='\0';
    strcpy(dest, retezec);
}

void upraveniNazvu (char* jidlo) {
    const int delka_nazvu = strlen(jidlo);

    if(delka_nazvu > MAX_DELKA_RADKU) {
        int vyskyt_carky = (int)(strrchr(jidlo,',')-jidlo+1);

        if (vyskyt_carky>delka_nazvu/3) { //deleni podle carky
            char cast1[vyskyt_carky];
            char cast2[delka_nazvu-vyskyt_carky];
            kopiruj(cast1, jidlo, 0, vyskyt_carky-1);
            kopiruj(cast2, jidlo, vyskyt_carky, delka_nazvu);
            if (strlen(cast1)>MAX_DELKA_RADKU) {
            	vyskyt_carky = (int)(strrchr(cast1,',')-cast1+1);
            	char cast3[vyskyt_carky];
            	cast2[0] = '\0';
            	kopiruj(cast3, jidlo, 0, vyskyt_carky-1);
            	kopiruj(cast2, jidlo, vyskyt_carky, delka_nazvu);
            	vlozeniRadku(cast3);
            	vlozeniRadku(cast2);
            }
            else {
            vlozeniRadku(cast1);
            vlozeniRadku(cast2);
            }
        }

        else { //deleni podle mezery
        char *cast;
        int delka_casti = 0;
        char temp_jidlo[delka_nazvu];
        strcpy(temp_jidlo, jidlo);
        cast = strtok(temp_jidlo, " ");
        char cast1[MAX_DELKA_RADKU+1];
        char cast2[(delka_nazvu-MAX_DELKA_RADKU)+15];
            while( cast != NULL ) {
                delka_casti += strlen(cast)+1;
                if (delka_casti>MAX_DELKA_RADKU) {
                    strcat(cast2, cast);
                    strcat(cast2, " ");
                }
                else {
                    strcat(cast1, cast);
                    strcat(cast1, " ");
                }
                cast = strtok(NULL, " ");
            }
            cast1[strlen(cast1)-1] = '\0';
            cast2[strlen(cast2)-1] ='\0';
        vlozeniRadku(cast1);
        vlozeniRadku(cast2);
        }
        
    }
    else {
        vlozeniRadku(jidlo);
    }
}

void vymazPoleRetezcu (char** pole_retezcu, int velikost_pole) {
    for(int i=0;i<velikost_pole;i++) {
        free(pole_retezcu[i]);
    }
    free(pole_retezcu);
}

void nactiJidla (int podsystem, int kategorie) {
	jidlaRadky=calloc(1, sizeof(char*)); //inicializace pole jidlaRadky
	pocet_radku=0;

    char url[MAX_DELKA_URL];
    url[0] ='\0';
    strcat(url,ZACATEK_URL);
    strcat(url, "GetJidla");
    strcat(url, "&api=");
    strcat(url, KLIC);
    strcat(url, "&Podsystem=");
    char podsystem_char[4];
    sprintf(podsystem_char, "%d", podsystem);
    strcat(url, podsystem_char); //hotova url

    nactiUrl(url);

     // parsovani JSON
    cJSON *json = cJSON_Parse(chunk.memory);
    if (json == NULL) {
        cJSON_Delete(json);
    }

    int n = cJSON_GetArraySize(json);
    for (int i = 0; i < n; i++) {
        cJSON *jidlo_json= cJSON_GetArrayItem(json, i);
        if (cJSON_GetObjectItemCaseSensitive(jidlo_json, "kategorie")->valueint==kategorie) {
            upraveniNazvu(cJSON_GetObjectItemCaseSensitive(jidlo_json, "nazev")->valuestring);
            char radek[MAX_DELKA_RADKU];
            radek[0] ='\0';
            char cena_char[6];
            strcat(radek, "<i>");
            sprintf(cena_char, "%.0f", cJSON_GetObjectItemCaseSensitive(jidlo_json, "cena")->valuedouble);
            strcat(radek, cena_char);
            strcat(radek, ",- Kč/");
            sprintf(cena_char, "%.0f", cJSON_GetObjectItemCaseSensitive(jidlo_json, "cena_stud")->valuedouble);
            strcat(radek, cena_char);
            strcat(radek, ",- Kč, ");
            if (cJSON_GetObjectItemCaseSensitive(jidlo_json, "vaha")->valuestring!= NULL) {
                strcat(radek, cJSON_GetObjectItemCaseSensitive(jidlo_json, "vaha")->valuestring);
            }
            else {
                strcat(radek, "----");
            }
            strcat(radek, "</i>");
            vlozeniRadku(radek);
        }
    }
    cJSON_Delete(json);
}

void nactiKategorie (int podsystem) {
	kategorie=calloc(15, sizeof(KATEGORIE)); //inicializace pole kategorie

	char url[MAX_DELKA_URL];
	url[0] ='\0';
	strcat(url,ZACATEK_URL);
	strcat(url, "GetKategorie");
	strcat(url, "&api=");
	strcat(url, KLIC);
	strcat(url, "&Podsystem=");
	char podsystem_char[4];
	sprintf(podsystem_char, "%d", podsystem);
	strcat(url, podsystem_char); //hotova url

	nactiUrl(url);

	cJSON *json = cJSON_Parse(chunk.memory);
	if (json == NULL) {
		cJSON_Delete(json);
	}

	int n = cJSON_GetArraySize(json);
	pocet_kategorii=n;
	for (int i = 0; i < n; i++) {
		cJSON *kategorie_json= cJSON_GetArrayItem(json, i);
	    KATEGORIE kat;
	    char* nazev = calloc(60, sizeof(char));
	    strcpy(nazev, cJSON_GetObjectItemCaseSensitive(kategorie_json, "nazev")->valuestring);
	    kat.nazev=nazev;
	    kat.id=cJSON_GetObjectItemCaseSensitive(kategorie_json, "id")->valueint;
	    kategorie[i]=kat;
	 }
	cJSON_Delete(json);
}

int najdiID(char* kat) {
	for(int i=0;i<pocet_kategorii;i++) {
		if (strcmp(kat, kategorie[i].nazev)==0) {
			return kategorie[i].id;
		}
	}
	return -1;
}
