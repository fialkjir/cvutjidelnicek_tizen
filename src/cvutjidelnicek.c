#include <tizen.h>
#include "cvutjidelnicek.h"

typedef struct widget_instance_data {
	Evas_Object *win;
	Evas_Object *label;
	Evas_Object *lbl_menza;
	Evas_Object *lbl_kategorie;
	Evas_Object *box;
	Evas_Object *btn_predchozi;
	Evas_Object *btn_dalsi;
	Evas_Object* jidelak[20];
	Evas_Object *table;
	Evas_Object *conform;

} widget_instance_data_s;

//glob promene
widget_instance_data_s *wid;
int stranka;

void
prepisText (int poradi_kategorie) {
	char nazev_kategorie[60]="";
	strcat(nazev_kategorie, "<font_size=16><text_align=center><b>");
	strcat(nazev_kategorie, kategorie[poradi_kategorie].nazev);
	strcat(nazev_kategorie, "</b>");
	elm_object_text_set(wid->lbl_kategorie, nazev_kategorie);

	vymazPoleRetezcu(jidlaRadky, pocet_radku);
	nactiJidla(ID_TECHNICKA, kategorie[poradi_kategorie].id);
	for(int i=0;i<MAX_RADKU;i++) {
		if (i>pocet_radku-1) {
			elm_object_text_set(wid->jidelak[i], "<font_size=14> ");
		}
		else {
			elm_object_text_set(wid->jidelak[i], jidlaRadky[i]);
		}
	}
}

void
clicked_btn_pred(void *data, Evas_Object *obj, void *event_info)
{
	if(stranka!=0) {
    stranka=stranka-1;
    prepisText(stranka);
	}
}

void
clicked_btn_dalsi(void *data, Evas_Object *obj, void *event_info)
{
	if(stranka!=pocet_kategorii-1) {
    stranka=stranka+1;
    prepisText(stranka);
	}
}

static int
widget_instance_create(widget_context_h context, bundle *content, int w, int h, void *user_data)
{
	stranka=0;
	wid = (widget_instance_data_s*) malloc(sizeof(widget_instance_data_s));

	nactiKategorie(ID_TECHNICKA);
	nactiJidla(ID_TECHNICKA, najdiID("Hlavní jídla"));

	widget_app_get_elm_win(context, &wid->win);
	evas_object_resize(wid->win, w, h);

	wid->conform = elm_conformant_add(wid->win);
	evas_object_size_hint_weight_set(wid->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(wid->win, wid->conform);
	evas_object_show(wid->conform);

	wid->table = elm_table_add(wid->conform);
	elm_object_content_set(wid->conform, wid->table);
	evas_object_size_hint_weight_set(wid->table, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(wid->table, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_table_padding_set(wid->table, 1, 1);
	evas_object_show(wid->table);

	wid->label = elm_label_add(wid->table);
	elm_object_text_set(wid->label, "<font_size=30><text_align=center> ");
	elm_table_pack(wid->table, wid->label, 0, 0, 2, 1);
	evas_object_show(wid->label);

	wid->lbl_menza = elm_label_add(wid->table);
	elm_object_text_set(wid->lbl_menza, "<font_size=16><text_align=center><b>Technická menza</b>");
	elm_table_pack(wid->table, wid->lbl_menza, 0, 1, 2, 1);
	evas_object_show(wid->lbl_menza);

	wid->lbl_kategorie = elm_label_add(wid->table);
	elm_object_text_set(wid->lbl_kategorie, "<font_size=16><text_align=center><b>Hlavní jídla</b>");
	elm_table_pack(wid->table, wid->lbl_kategorie, 0, 2, 2, 1);
	evas_object_show(wid->lbl_kategorie);

	//vytvoreni layoutu box
	wid->box = elm_box_add(wid->table);
	evas_object_show(wid->box);
	evas_object_size_hint_weight_set(wid->box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_table_pack(wid->table, wid->box, 0, 3, 2, 1);
	//forcyklus pro labely
	for(int i=0;i<MAX_RADKU;i++) {
		wid->jidelak[i] = elm_label_add(wid->box);
		evas_object_show(wid->jidelak[i]);
		if (i>pocet_radku-1) {
			elm_object_text_set(wid->jidelak[i], "<font_size=14> ");
		}
		else {
			elm_object_text_set(wid->jidelak[i], jidlaRadky[i]);
		}
		elm_box_pack_end(wid->box, wid->jidelak[i]);
	}
 //dvojice ovladacich  tlacitek
	wid->btn_predchozi = elm_button_add(wid->table);
	elm_object_text_set(wid->btn_predchozi, "<font_size=16><align=right>Předchozí</align>");
	evas_object_size_hint_weight_set(wid->btn_predchozi, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(wid->btn_predchozi, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_style_set(wid->btn_predchozi, "bottom");
	elm_table_pack(wid->table, wid->btn_predchozi, 0, 4, 1, 1);
	evas_object_smart_callback_add(wid->btn_predchozi, "clicked", clicked_btn_pred, NULL);
	evas_object_show(wid->btn_predchozi);

	wid->btn_dalsi = elm_button_add(wid->table);
	elm_object_text_set(wid->btn_dalsi, "<font_size=16><align=left>     Další</align>");
	evas_object_size_hint_weight_set(wid->btn_dalsi, 0.1, 0.1);
	evas_object_size_hint_align_set(wid->btn_dalsi, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_table_pack(wid->table, wid->btn_dalsi, 1, 4, 1, 1);
	elm_object_style_set(wid->btn_dalsi, "bottom");
	evas_object_smart_callback_add(wid->btn_dalsi, "clicked", clicked_btn_dalsi, NULL);
	evas_object_show(wid->btn_dalsi);

	evas_object_show(wid->win);
	widget_app_context_set_tag(context, wid);

	return WIDGET_ERROR_NONE;
}

static int
widget_instance_destroy(widget_context_h context, widget_app_destroy_type_e reason, bundle *content, void *user_data)
{
	widget_instance_data_s *wid = NULL;
	widget_app_context_get_tag(context,(void**)&wid);

	if (wid->win)
	evas_object_del(wid->win);

	free(wid);
	vymazPoleRetezcu(jidlaRadky, pocet_radku);
	for(int i=0;i<pocet_radku;i++) {
			free(kategorie[i].nazev);
		}
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_pause(widget_context_h context, void *user_data)
{
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_resume(widget_context_h context, void *user_data)
{
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_update(widget_context_h context, bundle *content, int force, void *user_data)
{
	return WIDGET_ERROR_NONE;
}

static int widget_instance_resize(widget_context_h context, int w, int h, void *user_data)
{
	return WIDGET_ERROR_NONE;
}

static void widget_app_lang_changed(app_event_info_h event_info, void *user_data)
{
	char *locale = NULL;
	app_event_get_language(event_info, &locale);
	elm_language_set(locale);
	free(locale);
}

static void widget_app_region_changed(app_event_info_h event_info, void *user_data)
{

}

static widget_class_h
widget_app_create(void *user_data)
{
	app_event_handler_h handlers[5] = {NULL, };

	widget_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED],
		APP_EVENT_LANGUAGE_CHANGED, widget_app_lang_changed, user_data);
	widget_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
		APP_EVENT_REGION_FORMAT_CHANGED, widget_app_region_changed, user_data);

	widget_instance_lifecycle_callback_s ops = {
		.create = widget_instance_create,
		.destroy = widget_instance_destroy,
		.pause = widget_instance_pause,
		.resume = widget_instance_resume,
		.update = widget_instance_update,
		.resize = widget_instance_resize,
	};

	return widget_app_class_create(ops, user_data);
}

static void widget_app_terminate(void *user_data)
{
	free(wid);
	vymazPoleRetezcu(jidlaRadky, pocet_radku);
	for(int i=0;i<pocet_radku;i++) {
		free(kategorie[i].nazev);
	}
}

int
main(int argc, char *argv[])
{
	widget_app_lifecycle_callback_s ops = {0,};

	ops.create = widget_app_create;
	ops.terminate = widget_app_terminate;

	widget_app_main(argc, argv, &ops, NULL);

	return 0;
}



