#include <curl.h>


static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if(mem->memory == NULL) {
		return 0;
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

void nactiUrl (char* url) {
	CURL *curl;
	CURLcode curl_err;

	chunk.memory = malloc(1);
	chunk.size = 0;

	curl = curl_easy_init();

	connection_h connection;
	int conn_err;
	conn_err = connection_create(&connection);

	if (conn_err != CONNECTION_ERROR_NONE) {
		dlog_print(DLOG_INFO, "vystup", "pripojeni nefunguje");
	}
	else {
		dlog_print(DLOG_INFO, "vystup", "pripojeni funguje");
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	curl_err = curl_easy_perform(curl);
	if (curl_err!=CURLE_OK) {
		dlog_print(DLOG_INFO, "vystup", "nacteni stranky se nepodarilo");
	}
	else {
		dlog_print(DLOG_INFO, "vystup", "nacteni stranky se podarilo");
	}

	curl_easy_cleanup(curl);
	connection_destroy(connection);
}

